ASCIIDOCFLAGS := -a data-uri -a icons -a toc2 -a toclevels=2 -a max-width=55em --theme volnitsky

TXTDIR := book/
HTMLDIR := html/
DOCBOOKDIR := docbook/
PDFDIR := pdf/
BOOKNAME := compiled_book

TXTFILE  := book/$(BOOKNAME).txt
HTMLFILE := html/$(BOOKNAME).html
PAGEDHTMLDIR := pagedhtml/
DOCBOOKFILE := docbook/$(BOOKNAME).xml
PDFFILE := pdf/$(BOOKNAME).pdf

.PHONY: all
all: html pagedhtml pdf docbook

.PHONY: html
html: $(HTMLFILE)
$(HTMLFILE): $(TXTFILE)
	asciidoc -o $@ $(ASCIIDOCFLAGS) $^

.PHONY: pagedhtml
pagedhtml: $(PAGEDHTMLDIR)
$(BOOKNAME).chunked: $(TXTFILE)
	a2x --destination-dir ./ -f chunked --asciidoc-opts="$(ASCIIDOCFLAGS)" $^
$(PAGEDHTMLDIR): $(BOOKNAME).chunked
	cp -r $^ $@
 
.PHONY: pdf
pdf: $(PDFFILE)
$(PDFFILE): $(DOCBOOKFILE)
	dblatex -o $@ $^

.PHONY: docbook
docbook: $(DOCBOOKFILE)
$(DOCBOOKFILE): $(TXTFILE)
	asciidoc -b docbook -o $@ $(ASCIIDOCFLAGS) $^

$(TXTFILE): book/main.txt book/readonly_members.txt book/passkey.txt book/indices.txt 
	cat $^ > $@

.PHONY: clean
clean:
	rm -f $(TXTFILE) $(HTMLFILE) $(DOCBOOKFILE) $(PDFFILE) -r $(PAGEDHTMLDIR)

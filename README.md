The Lounge<C++> should write a book.
This is how we will do it:

# Book Coverage and Topics

The book will be a collection of articles on interesting C++(11) techniques,
tips, workarounds, patterns, rules, etc which have not been covered in-depth
elsewhere.
Pretty much anything which could or has been written in a blog post by Lounge
members.
The target audience is intermediate to advanced C++ users hoping to learn some
tricks.

Non-standard libraries can be covered if you feel they have a wide appeal or
provide a good example.
Boost is always acceptable.

# How to Help

A few ways a lounger can help at this moment:

- Write or adapt an article into the book
- Edit or add-onto an article. Communicate with the original author if possible.
- Figure out how to work asciidoc because Pubby is too stupid to understand it!

# Writing Guide

The book is divided up into "articles" which are mostly self-contained, but
contain links to other articles under a list of dependencies.
Each article gets a new file in the ./book/ directory.

In addition, each article has a few "use cases" which provide some examples
of the technique you're describing.
Real-world use cases are ideal.

A style guide is being written and can be found at style.txt.
